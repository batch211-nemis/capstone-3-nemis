import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Footer from './components/Footer';
import Home from './pages/Home';
import Products from './pages/Products';
import Products2 from './pages/Products2';
import ProductView from './components/ProductView';
import ProductsView2 from './components/ProductsView2';
import Checkout from './components/Checkout';
import AddOrder from './components/AddOrder';
import UserOrders from './components/UserOrders';
import UserAccount from './components/UserAccount';
import Dash from './components/Dash';
import DashRetUser from './components/DashRetUser';
import DashViewOrders from './components/DashViewOrders';
import DashViewProd from './components/DashViewProd';
import DashOrderDet from './components/DashOrderDet';
import DashAccount from './components/DashAccount';
import AddProduct from './components/AddProduct';
import EditProduct from './components/EditProduct';
import Register from './pages/Register';
import About from './pages/About';
import Contact from './pages/Contact';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import './App.css';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }


  useEffect(()=>{
    fetch('https://backend-csp3-nemis.onrender.com/users/details',{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{
      console.log(data);

      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        });
      }else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  },[]);

  return (

    <UserProvider value = {{user, setUser, unsetUser}}>
        <Router>
          <AppNavbar/>
          <Container>
            <Routes>
              <Route path="/" element={<Home/>}/>
              <Route path="/admin" element={<Dash/>}/>
              <Route path="/admin/retrieveallusers" element={<DashRetUser/>}/>
              <Route path="/admin/viewOrders" element={<DashViewOrders/>}/>
              <Route path="/admin/:productId" element={<DashViewProd/>}/>
              <Route path="/:orderId" element={<DashOrderDet/>}/>
              <Route path="/admin/adminAccount" element={<DashAccount/>}/>
              <Route path="/addProduct" element={<AddProduct/>} />
              <Route path="/editProduct/:productId" element={<EditProduct/>} />
              <Route path="/products" element={<Products/>}/>
              <Route path="/products2" element={<Products2/>}/>
              <Route path="/products/:productId" element={<ProductView/>}/>
              <Route path="/productsview/:productId" element={<ProductsView2/>}/>
              <Route path="/checkout/:productId" element={<Checkout/>}/>
              <Route path="/addorder/:productId" element={<AddOrder/>}/>
              <Route path="/userOrders" element={<UserOrders/>}/>
              <Route path="/userAccount" element={<UserAccount/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/about" element={<About/>}/>
              <Route path="/contact" element={<Contact/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="*" element={<Error/>}/>
            </Routes>
          </Container>
          <Footer/>
        </Router>
    </UserProvider>

  );
}

export default App;
