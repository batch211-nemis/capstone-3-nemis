import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function ProductView(){

	
	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(()=>{
		
		console.log(productId);
		fetch(`https://backend-csp3-nemis.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})

	},[productId])


	return(
		
		<Container className="mt-5 pt-5">
		   <Row className="justify-content-center">
		     <Col lg={{span:6}}>
				<Card className="no-gutters">
					<Card.Body >
						
						<Card.Title className="pt-3 fw-bolder">{name}</Card.Title>
						<Card.Subtitle className="pt-3 fw-bolder">Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle className="pt-3 fw-bolder">Price:</Card.Subtitle>
						<Card.Text className="fw-bolder">PHP {price}</Card.Text>
					</Card.Body>
					<Card.Footer className="text-muted">
						{
							(user.id!==null)?
							<Button as={Link} to={`/addorder/${productId}`}>Checkout</Button>
							:
							<Link className="btn btn-danger" to="/login">Log in to Order</Link>
						}
						</Card.Footer>
				</Card>
		     </Col>
		  </Row>
		</Container>

	)
}













