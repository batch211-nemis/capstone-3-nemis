import { useState, useEffect } from 'react';
import { Table, Container, Card, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';


export default function OrderDetails (){

	const { orderId } = useParams();


	const [userId, setUserId] = useState("");
	const [deliveryMode, setDeliveryMode ] = useState("");
	const [deliveryAddress, setDeliveryAddress] = useState("");
	const [totalAmount, setTotalAmount] = useState(0);
	const [purchasedOn, setPurchasedOn] = useState("");
	const [orderStatus, setOrderStatus] = useState("");
	

	function formatDate(isoDate) {
  	return (new Date(isoDate)).toLocaleString(
    'en-PH',
    {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false
    }
  )
};


	useEffect(()=>{
		
		
		fetch(`https://backend-csp3-nemis.onrender.com/orders/${orderId}`)
		.then(res=>res.json())
		.then(data=>{
			setUserId(data.userId);
			setDeliveryMode(data.deliveryMode);
			setDeliveryAddress(data.deliveryAddress);
			setPurchasedOn(data.purchasedOn);
			setOrderStatus(data.orderStatus);
			setTotalAmount(data.totalAmount);
		})

	},[orderId])


	const [viewOrder, setViewOrder] = useState([]);

	const fetchData = () =>{
	
	fetch(`https://backend-csp3-nemis.onrender.com/orders/${orderId}`,{
		headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			const arr = data.products;

			console.log(arr);

			setViewOrder(arr.map(item => {

				return (
					<tr>
						<td>{item.productId}</td>
						<td>{item.quantity}</td>
						<td>{item.subTotal}</td>
					</tr>
					
					)
				}))
		  	})
	}
 					
     
useEffect(()=>{
		
		fetchData();
	})



	return(
		
		<Container className="mt-5 pt-5">
		  <Row className="justify-content-center">
		     <Col lg={{span:6}}>
				<Card >
					<Card.Body >
						<Card.Title className="pt-2 fw-bolder">Order {orderId}</Card.Title>
						<hr />
						<Card.Subtitle className="pt-3 pb-2 fw-bolder">UserId: {userId} </Card.Subtitle>
						<p>Delivery Mode: {deliveryMode} <br />
						Delivery Address: {deliveryAddress} <br />
						Date of Purchase: {formatDate(purchasedOn)}</p>
						<Card.Subtitle className="fw-bolder" style={{color: 'green'}}>Order Status: {orderStatus}</Card.Subtitle>
					</Card.Body>
				</Card>
		     </Col>
		  </Row>

		  <hr />

		  {/*Table Start*/}
    				<div className=" min-vh-100  p-md-3 ">
    					<Table striped bordered hover size="sm" style={{width: "70%"}} className="pTable">
						     <thead className=" bg-dark text-light">
						       <tr className="tabhead">
						         <th>Product ID</th>
						         <th>Quantity</th>
						         <th>Subtotal</th>
						       </tr>
						       
						     </thead>
						     <tbody className="text-center">
						       { viewOrder }

						       <tr>
									<td className="fw-bolder" style={{textAlign: 'right'}} colspan="3">TOTAL AMOUNT: PHP {totalAmount}</td>
								</tr>

						     </tbody>

						   </Table>
    				</div>
    				{/*Table End*/}

		</Container>

	)
}

