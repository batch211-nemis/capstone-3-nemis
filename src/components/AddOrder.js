import { useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';

export default function AddOrder() {


	const { productId } = useParams();

	const navigate = useNavigate();
    const [quantity, setQuantity] = useState(1);
    const [subTotal, setSubTotal ] = useState(0);
    

   console.log(productId);

	function addOrder (e) {

	    e.preventDefault();

	    fetch(`https://backend-csp3-nemis.onrender.com/orders/addOrder`, {
           	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
            body: JSON.stringify({
            	productId:productId,
                quantity: quantity,
                subTotal: subTotal
			})
	    })
	    .then(res => res.json())
        .then((data) => {

        	console.log(data);

            if(data){
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Added Order completed'
                })
	    
                navigate("/userOrders");
            }
            else {
                Swal.fire({
                    title: "Failed",
                    icon: 'error',
                    text: 'Failed to order'
      			});
	    	}

	    })

	    setQuantity(1);
	}


    return (
			<>
		    	<h1 className="my-5 text-center pt-5">Place an Order</h1>
		        <Form onSubmit={(e) => addOrder (e)}>

		        	<Form.Group controlId="productId" className="mb-3">
		                <Form.Label>Product ID</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Product ID" 
			                value = {productId}
			                onChange={e => productId(e.target.value)}
			                disabled
		                />
		            </Form.Group>

		        	<Form.Group controlId="quantity" className="mb-3">
		                <Form.Label>Quantity</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Quantity" 
			                value = {quantity}
			                onChange={e => setQuantity(e.target.value)}
			                required
		                />
		            </Form.Group>

		           <Form.Group controlId="subTotal" className="mb-3">
		                <Form.Label>SubTotal</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="SubTotal" 
			                value = {subTotal}
			                onChange={e => setSubTotal(e.target.value)}
			                disabled
		                />
		            </Form.Group>


	        	 
	        	    	<Button variant="primary" type="submit" id="submitBtn">Place Order</Button>
	        
	        	    	<Button className="m-2" as={Link} to={`/products2/${productId}`} variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>    	
    )

}



