import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Button } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function UserAccount ({e}){


	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [_id, set_id] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState(0);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [mobileNo, setMobileNo] = useState(0);

	useEffect(()=>{
		
		fetch('https://backend-csp3-nemis.onrender.com/users/userDetails', {
          headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{

			console.log(data);

			set_id(data._id);
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setPassword(data.password);
			setMobileNo(data.mobileNo);

		})

	},[])


	function editUser(e) {

	    e.preventDefault();

	    fetch(`https://backend-csp3-nemis.onrender.com/users/updateUser`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: _id,
			   	firstName: firstName,
			    lastName: lastName,
			    email: email,
			    mobileNo: mobileNo,
			    password: password
			    
			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Succesfully Updated",
	    		    icon: "success",
	    		    text: "Account information is now updated"
	    		});

	    		navigate("/userOrders");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	}


	return(
	
		!(user.isAdmin)
		?
			<Container className="container-fluid">
    			<div className="pt-3">

    			<Card className="pt-5">

    				<h1 className="pt-5 text-center">My Account Information</h1>
    				
		    	<hr/>

		    	
		        <Form className="container-fluid p-5" onSubmit={(e) => editUser(e)}>
		        
		        	<Form.Group controlId="userId" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels ms-3">User ID:</Form.Label>
		                <Form.Control className="form-controls"
			                type="text" 
			                value = {_id}
			    						disabled
		                />
		            </Form.Group>


		            <Form.Group controlId="firstName" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels">Firstname:</Form.Label>
		                <Form.Control className="form-controls"
		                	type="text"
			                value = {firstName}
			                onChange={e => setFirstName(e.target.value)}
			                
		                />
		            </Form.Group>

		            <Form.Group controlId="lastName" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels">Lastname:</Form.Label>
		                <Form.Control className="form-controls"
			                type="text" 
			                value = {lastName}
			           			onChange={e => setLastName(e.target.value)}
		                />
		            </Form.Group>

		            <Form.Group controlId="email" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels ms-4">Email</Form.Label>
		                <Form.Control className="form-controls"
			                type="text" 
			                value = {email}
			            		onChange={e => setEmail(e.target.value)}
		                />
		            </Form.Group>

		            <Form.Group controlId="password" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels">Password:</Form.Label>
		                <Form.Control className="form-controls"
			                type="password"  
			                value = {password}
			            		onChange={e => setPassword(e.target.value)}
			            		disabled
		                />
		            </Form.Group>

		                <Form.Group controlId="mobileNo" className="mb-3 pb-4 d-flex justify-content-around">
		                <Form.Label className="form-labels">Mobile Number:</Form.Label>
		                <Form.Control className="mx-4 form-controls"
			                type="text" 
			                value = {mobileNo}
			            		onChange={e => setMobileNo(e.target.value)}
		                />
		            </Form.Group>

		         	<div className="text-center">
	        	    	<Button className="me-2" variant="success" type="submit" id="submitBtn">
	        	    		Update
	        	    	</Button>
	        	         
	        	    	<Button className="my-2"as={Link} to="/userOrders" variant="danger">
	        	    		Cancel
	        	    	</Button>
	        	   </div>
	        	  

		        </Form>
		        </Card>

    			</div>	
    		</Container>
    			
		:
		<Navigate to="/products" />
		
	)
}
