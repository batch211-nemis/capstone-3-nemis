import { useContext, useEffect } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import logo from '../logo.png';


export default function AppNavbar(){

	const { user } = useContext(UserContext);


	useEffect(()=>{
		
		fetch('https://backend-csp3-nemis.onrender.com/users/details',{
		headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
		.then(res=>res.json())
		.then(data=>{
			console.log(data);
			
		})

	},[])

	return(
		<Navbar  expand="lg" className="bg-dark fixed-top container-fluid">
	      <Container className="ms-auto">
	        <Navbar.Brand className="text-light" as={Link} to="/"><img src={logo} width="12%" alt="Mrs.Nemis Dry Sotanghon"/></Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	       
	          <Nav className="d-flex">
	            <Nav.Link className="text-light" as={Link} to="/">Home</Nav.Link>
	        	<Nav.Link className="text-light" as={Link} to="/products">Products</Nav.Link>
	        	<Nav.Link className="text-light" as={Link} to="/about">About Us</Nav.Link>
	        	<Nav.Link className="text-light" as={Link} to="/contact">Contact Us</Nav.Link>
	          </Nav>

	        <Nav className="ms-auto">

	            {(user.id !== null) && !(user.isAdmin)  ?
	            <>
	            	<Nav.Link className="text-light ms-auto" as={Link} to="/userOrders">My Orders</Nav.Link>
	            	<Nav.Link className="text-light ms-auto" as={Link} to="/logout">Logout</Nav.Link>
	         	</>
	         	:
	            (user.isAdmin) ?
	            	<>
	            	<Nav.Link className="text-light ms-auto" as={Link} to="/admin">Dashboard</Nav.Link>
	            	<Nav.Link className="text-light ms-auto" as={Link} to="/logout">Logout</Nav.Link>
	            	</>
	            	:
	            	<>
	            		<Nav.Link className="text-light ms-auto" as={Link} to="/login">Login</Nav.Link>
	            		<Nav.Link className="text-light ms-auto" as={Link} to="/register">Register</Nav.Link>
	            	</>
	            	}
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}