import { useContext, useState, useEffect } from "react";
import {Table, Button, Container} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import { FaUsers } from "react-icons/fa";
import { FaShoppingBag } from "react-icons/fa";
import { FaShoppingCart } from "react-icons/fa";
import { FaUsersCog } from "react-icons/fa";
import Swal from "sweetalert2";

export default function Dash(){


	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	
	const fetchData = () =>{

		fetch(`https://backend-csp3-nemis.onrender.com/products/`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td><img src={product.photo} width={200} height={200} alt="Mrs.Nemis Dry-Sotanghon"/></td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td className="tablecol">
							{
								// We use conditional rendering to set which button should be visible based on the course status (active/inactive)
								(product.isActive)
								?	
								 	<>
									<Button variant="danger" size="sm" className="my-3 mx-3" onClick ={() => archive(product._id, product.name)}>Archive</Button>
									<Button variant="success" as={ Link } to={`/editProduct/${product._id}`}  size="sm" className="my-3 mx-3" >Edit</Button>
									<Button variant="primary" as={ Link } to={`/admin/${product._id}`}  size="sm" className="my-3 mx-3">View</Button>
									</>
								:
											
										<Button variant="success" className="my-3 mx-3" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
											
							}
						</td>
					</tr>
				)
			}))

		})
	}

	//Making the course inactive
	const archive = (productId, productName) =>{
		//console.log(productId);
		//console.log(productName);

		fetch(`https://backend-csp3-nemis.onrender.com/products/archiveProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the course active
	const unarchive = (productId, productName) =>{
		//console.log(productId);
		//console.log(productName);

		fetch(`https://backend-csp3-nemis.onrender.com/products/activateProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	useEffect(()=>{
		
		fetchData();
	})
	

	const [firstName, setFirstName] = useState("");

	useEffect(()=>{
		
		fetch('https://backend-csp3-nemis.onrender.com/users/userDetails', {
          headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{

			console.log(data);

			setFirstName(data.firstName);
			

		})

	},[])




	return(
	
		(user.id !== null) && (user.isAdmin)  
		?
		<>
			{/*Main Content Section Start*/}
			<Container className="dashcon container-fluid">
    	<div className="  text-center pt-5">
    		{/*<!-- row -->*/}
    		<div className="row">
    			{/*<!-- Side-bar Section Start -->*/}

    			<div className=" card col-xl-3 d-none d-xl-block pt-5 ">
    				<div>
    					{/*<!-- Users Menu Start-->*/}
    					<h3>Admin Dashboard</h3>
    					<h4 className="pb-4">Welcome {firstName}!</h4>
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">

    								<Link to="/admin/retrieveallusers" style={{textDecoration: 'none'}} className="text-light p-2"><FaUsers className="p-2" style={{ fontSize: '60px'}}/>
    									Users
    								</Link>

    							</h4> 
    						</div>
  
    					</div>
    					{/*<!-- Users Menu End-->*/}

    					{/*<!-- Products Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin" className="text-light" style={{textDecoration: 'none'}}><FaShoppingBag className="p-2" style={{ fontSize: '60px'}}/>

    									Products
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Products Menu End -->*/}

    					{/*<!-- Orders Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin/viewOrders" className="text-light" style={{textDecoration: 'none'}} ><FaShoppingCart className="p-2" style={{ fontSize: '60px'}}/>

    									Orders
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Orders Menu End -->*/}

    					{/*<!-- My Account Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to={`/admin/adminAccount`} className="text-light" style={{textDecoration: 'none'}}><FaUsersCog className="p-2" style={{ fontSize: '60px'}}/>

    									My Account
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- My Account Menu End -->*/}
    				</div>
    			</div>
    			{/*Side Bar End*/}

    			{/*Right Side Start*/}
    			<div className="col-xl-9 pt-5">

    			<div className="prodtext">
    			<h1 >Products</h1>
    			</div>

				<div className="prodbtn">

					<Button as={Link} to="/AddProduct" variant="primary"  className="me-2">Add Product</Button>
					<Button as={Link} to="/admin/viewOrders" variant="primary"  className="me-2">Show Orders</Button>
				

				</div>
    				{/*Table Start*/}
    				<div className=" min-vh-100  p-md-3">
    					<Table striped bordered hover size="sm">
						     <thead className=" bg-dark text-light">
						       <tr className="tabhead">
						         <th>Product ID</th>
						         <th>Product Name</th>
						         <th>Product Image</th>
						         <th>Description</th>
						         <th>Price</th>
						         <th>Status</th>
						         <th>Action</th>
						       </tr>
						     </thead>
						     <tbody>
						       { allProducts }
						     </tbody>
						   </Table>
    				</div>
    				{/*Table End*/}

    			</div>	
    			{/*Right Side End*/}

    		</div>
    		{/*<!-- row -->*/}
    	</div>	
    	</Container>
		</>
		:
		<Navigate to="/products" />
		
	)
}