import { useContext, useState, useEffect } from "react";
import {Table, Button, Container} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import { FaUsers } from "react-icons/fa";
import { FaShoppingBag } from "react-icons/fa";
import { FaShoppingCart } from "react-icons/fa";
import { FaUsersCog } from "react-icons/fa";

import Swal from "sweetalert2";

export default function Dash(){

	const {user} = useContext(UserContext);

	
	const [allUsers, setAllUsers] = useState([]);


	const fetchData = () =>{
	
		fetch(`https://backend-csp3-nemis.onrender.com/users/allusers`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllUsers(data.map(user => {
				return(
					<tr key={user._id}>
						<td>{user._id}</td>
						<td>{user.firstName}</td>
						<td>{user.lastName}</td>
						<td>{user.email}</td>
						<td>{user.mobileNo}</td>
						<td>{user.isAdmin ? "True" : "False"}</td>
						<td className="tablecol">
							{
								
								(user.isAdmin)
								?	
								 	
									<Button variant="danger" size="sm" className="my-3 mx-3" onClick ={() => unSet(user._id, user.name)}>Make a User</Button>

								:

									<Button variant="success" className="my-3 mx-3" size="sm" onClick ={() => setAdmin(user._id, user.name)}>Set as Admin</Button>
									
							}
							</td>
					</tr>
				)
			}))

		})
	}

	//Making user an admin
	const setAdmin = (userId) =>{
			

		fetch(`http://localhost:4000/users/updateAdmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Succesful!",
					icon: "success",
					text: `${data.firstName} is now an Admin.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making user not admin
	const unSet = (userId) =>{

		fetch(`http://localhost:4000/users/unSetAdmin/${userId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Succesful!",
					icon: "success",
					text: `${data.firstName} was changed from admin to user.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}
	
	
	useEffect(()=>{
	
		fetchData();
	})


	const [firstName, setFirstName] = useState("");

	
	useEffect(()=>{
		
		fetch('http://localhost:4000/users/userDetails', {
          headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{

			console.log(data);

			setFirstName(data.firstName);


		})

	},[])
	
	return(
	
		(user.isAdmin)
		?
		<>
			{/*Main Content Section Start*/}
			<Container className="dashcon container-fluid">
    	<div className="  text-center pt-5">
    		{/*<!-- row -->*/}
    		<div className="row">
    					{/*<!-- Side-bar Section Start -->*/}

    			<div className=" card col-xl-3 d-none d-xl-block pt-5 ">
    				<div>
    					{/*<!-- Users Menu Start-->*/}
    					<h3>Admin Dashboard</h3>
    					<h4 className="pb-4">Welcome {firstName}!</h4>
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">

    								<Link to="/admin/retrieveallusers" style={{textDecoration: 'none'}} className="text-light p-2"><FaUsers className="p-2" style={{ fontSize: '60px'}}/>
    									Users
    								</Link>

    							</h4> 
    						</div>
  
    					</div>
    					{/*<!-- Users Menu End-->*/}

    					{/*<!-- Products Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin" className="text-light" style={{textDecoration: 'none'}}><FaShoppingBag className="p-2" style={{ fontSize: '60px'}}/>

    									Products
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Products Menu End -->*/}

    					{/*<!-- Orders Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin/viewOrders" className="text-light" style={{textDecoration: 'none'}} ><FaShoppingCart className="p-2" style={{ fontSize: '60px'}}/>

    									Orders
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Orders Menu End -->*/}

    					{/*<!-- My Account Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to={`/admin/adminAccount`} className="text-light" style={{textDecoration: 'none'}}><FaUsersCog className="p-2" style={{ fontSize: '60px'}}/>

    									My Account
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- My Account Menu End -->*/}
    				</div>
    			</div>
    			{/*Side Bar End*/}

    			{/*Right Side Start*/}
    			<div className="col-xl-9 pt-5">

    			<div className="prodtext">
    			<h1 >Users Details</h1>

				</div>
    				{/*Table Start*/}
    				<div className=" min-vh-100  p-md-3">
    					<Table striped bordered hover size="sm">
						     <thead className=" bg-dark text-light">
						       <tr className="tabhead">
						         <th>User ID</th>
						         <th>First Name</th>
						         <th>Last Name</th>
						         <th>Email</th>
						         <th>Mobile No.</th>
						         <th>isAdmin</th>
						         <th>Action</th>
						       </tr>
						     </thead>
						     <tbody>
						       { allUsers }
						     </tbody>
						   </Table>
    				</div>
    				{/*Table End*/}

    			</div>	
    			{/*Right Side End*/}

    		</div>
    		{/*<!-- row -->*/}
    	</div>	
    	</Container>
		</>
		:
		<Navigate to="/products" />
		
	)
}
