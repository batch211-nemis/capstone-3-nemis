import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';

export default function ProductCard({productProp}){

	let { name, description, price, _id } = productProp;


return (
  <div>
    <Card className="no-gutters" style={{width:"100%", height:"100%"}}>
 
      <Card.Body >
        <Card.Title className="pt-3 fw-bolder">{name}</Card.Title>
        <Card.Subtitle className="pt-3 fw-bolder" >Description:</Card.Subtitle>
        <Card.Text className="card-text">
          {description}
        </Card.Text>
        <Card.Subtitle className="fw-bolder pt-3">Price:</Card.Subtitle>
		<Card.Text className="fw-bolder">PHP {price}</Card.Text>
      </Card.Body>
     <Card.Footer className=" style={{ margin-left: 0, margin-right: 0 }}" >
		<Button as={Link} to={`/productsview/${_id}`}>Details</Button>
	</Card.Footer>
    </Card >
  
    </div>

  )
}





