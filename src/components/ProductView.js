import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductView(){

	
	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [photo, setPhoto] = useState("");

	useEffect(()=>{
		
		console.log(productId);
		fetch(`https://backend-csp3-nemis.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setPhoto(data.photo);
		})

	},[productId])


	return(
		
		<Container className="mt-5 pt-5 d-flex"  style={{width:"100%", height:"100%"}}>

			    <img className="photo2"  src={photo} alt={name}/>
			 	
		   <Row className="justify-content-around">

			   <Col className="px-5">

				<Card className="card no-gutters shadow-outer">
					<Card.Body >
					<div>
					</div>
						<Card.Title className="pt-3 fw-bolder">{name}</Card.Title>
						<Card.Subtitle className="pt-3 fw-bolder">Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle className="pt-3 fw-bolder">Price:</Card.Subtitle>
						<Card.Text className="fw-bolder">PHP {price}</Card.Text>
					
					</Card.Body>
					<Card.Footer className="text-muted">
						{
							(user.id!==null)?
							<Button as={Link} to={`/checkout/${productId}`}>Checkout</Button>
							:
							<Link className="btn btn-danger" to="/login">Log in to Order</Link>
						}
						</Card.Footer>

				</Card>
				
		     </Col>
		  </Row>
		</Container>

	)
}




