import { useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

import { Form, Button } from 'react-bootstrap';

export default function Checkout() {


	const { productId } = useParams();
	const navigate = useNavigate();
    const [quantity, setQuantity] = useState(1);
    const [deliveryMode, setDeliveryMode ] = useState("");
    const [deliveryAddress, setDeliveryAddress] = useState("");

   console.log(productId);

	function checkout(e) {

	    e.preventDefault();

	    fetch(`https://backend-csp3-nemis.onrender.com/orders/order`, {
           	method: "POST",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
            body: JSON.stringify({
            	productId: productId,
                quantity: quantity,
                deliveryMode: deliveryMode,
                deliveryAddress: deliveryAddress
			})
	    })
	    .then(res => res.json())
        .then((data) => {

        	console.log(data);

            if(data){
                Swal.fire({
                    title: 'Success!',
                    icon: 'success',
                    text: 'Order completed'
                })
	    
                navigate("/userOrders");
            }
            else {
                Swal.fire({
                    title: "Failed",
                    icon: 'error',
                    text: 'Failed to order'
      			});
	    	}

	    })

	    setQuantity(1);
        setDeliveryMode('');
        setDeliveryAddress('');
	}


    return (
			<>
		    	<h1 className="my-5 text-center pt-5">Place an Order</h1>
		        <Form onSubmit={(e) => checkout(e)}>

		        	<Form.Group controlId="productId" className="mb-3">
		                <Form.Label>Product ID</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Product ID" 
			                value = {productId}
			                onChange={e => productId(e.target.value)}
			                disabled
		                />
		            </Form.Group>

		        	<Form.Group controlId="quantity" className="mb-3">
		                <Form.Label>Quantity</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Quantity" 
			                value = {quantity}
			                onChange={e => setQuantity(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="deliveryMode" className="mb-3">
		                <Form.Label>Delivery Mode</Form.Label>
		                <Form.Control
		                	type="text"
			                placeholder="Delivery Mode" 
			                value = {deliveryMode}
			                onChange={e => setDeliveryMode(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="deliveryAddress" className="mb-3">
		                <Form.Label>Delivery Address</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Delivery Address" 
			                value = {deliveryAddress}
			                onChange={e => setDeliveryAddress(e.target.value)}
			                required
		                />
		            </Form.Group>

	        	 
	        	    	<Button variant="primary" type="submit" id="submitBtn">Place Order</Button>
	        
	        	    	<Button className="m-2" as={Link} to={`/products/${productId}`} variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>    	
    )

}



