import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Button } from 'react-bootstrap';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { FaUsers } from "react-icons/fa";
import { FaShoppingBag } from "react-icons/fa";
import { FaShoppingCart } from "react-icons/fa";
import { FaUsersCog } from "react-icons/fa";

export default function UserDetails ({e}){

	
	const { user } = useContext(UserContext);


	const navigate = useNavigate();

	const [_id, set_id] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState(0);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [mobileNo, setMobileNo] = useState(0);

	useEffect(()=>{
		
		fetch('https://backend-csp3-nemis.onrender.com/users/userDetails', {
          headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{

			console.log(data);

			set_id(data._id);
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setEmail(data.email);
			setPassword(data.password);
			setMobileNo(data.mobileNo);

		})

	},[])


	function editUser(e) {

	    e.preventDefault();

	    fetch(`https://backend-csp3-nemis.onrender.com/users/updateUser`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
					userId: _id,
			   	firstName: firstName,
			    lastName: lastName,
			    email: email,
			    mobileNo: mobileNo,
			    password: password
			    
			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Succesfully Updated",
	    		    icon: "success",
	    		    text: "Account information is now updated"
	    		});

	    		navigate("/admin/retrieveallusers");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	}



	return(
	
		(user.isAdmin)
		?
		<>
			{/*Main Content Section Start*/}
			<Container >
    	<div className="  text-center pt-5">
    		{/*<!-- row -->*/}
    		<div className="row">
    					{/*<!-- Side-bar Section Start -->*/}

    			<div className=" card col-xl-3 d-none d-xl-block pt-5 ">
    				<div>
    					{/*<!-- Users Menu Start-->*/}
    					<h3>Admin Dashboard</h3>
    					<h4 className="pb-4">Welcome {firstName}!</h4>
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">

    								<Link to="/admin/retrieveallusers" style={{textDecoration: 'none'}} className="text-light p-2"><FaUsers className="p-2" style={{ fontSize: '60px'}}/>
    									Users
    								</Link>

    							</h4> 
    						</div>
  
    					</div>
    					{/*<!-- Users Menu End-->*/}

    					{/*<!-- Products Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin" className="text-light" style={{textDecoration: 'none'}}><FaShoppingBag className="p-2" style={{ fontSize: '60px'}}/>

    									Products
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Products Menu End -->*/}

    					{/*<!-- Orders Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin/viewOrders" className="text-light" style={{textDecoration: 'none'}} ><FaShoppingCart className="p-2" style={{ fontSize: '60px'}}/>

    									Orders
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Orders Menu End -->*/}

    					{/*<!-- My Account Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to={`/admin/adminAccount`} className="text-light" style={{textDecoration: 'none'}}><FaUsersCog className="p-2" style={{ fontSize: '60px'}}/>

    									My Account
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- My Account Menu End -->*/}
    				</div>
    			</div>
    			{/*Side Bar End*/}



    			{/*Right Side Start*/}
    			<div className="col-xl-9 pt-3">

    			<Card>

    				<h1 className="pt-5">My Account Information</h1>
    				
		    	<hr/>

		    	
		        <Form className="container-fluid p-5" onSubmit={(e) => editUser(e)}>
		        
		        	<Form.Group controlId="userId" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels ms-3">User ID:</Form.Label>
		                <Form.Control className="form-controls"
			                type="text" 
			                value = {_id}
			    						disabled
		                />
		            </Form.Group>


		            <Form.Group controlId="firstName" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels">Firstname:</Form.Label>
		                <Form.Control className="form-controls"
		                	type="text"
			                value = {firstName}
			                onChange={e => setFirstName(e.target.value)}
			                
		                />
		            </Form.Group>

		            <Form.Group controlId="lastName" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels">Lastname:</Form.Label>
		                <Form.Control className="form-controls"
			                type="text" 
			                value = {lastName}
			           			onChange={e => setLastName(e.target.value)}
		                />
		            </Form.Group>

		            <Form.Group controlId="email" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels ms-4">Email</Form.Label>
		                <Form.Control className="form-controls"
			                type="text" 
			                value = {email}
			            		onChange={e => setEmail(e.target.value)}
		                />
		            </Form.Group>

		            <Form.Group controlId="password" className="mb-3 d-flex justify-content-around">
		                <Form.Label className="form-labels">Password:</Form.Label>
		                <Form.Control className="form-controls"
			                type="password"  
			                value = {password}
			            		onChange={e => setPassword(e.target.value)}
			            		disabled
		                />
		            </Form.Group>

		                <Form.Group controlId="mobileNo" className="mb-3 pb-4 d-flex justify-content-around">
		                <Form.Label className="form-labels">Mobile Number:</Form.Label>
		                <Form.Control className="mx-4 form-controls"
			                type="text" 
			                value = {mobileNo}
			            		onChange={e => setMobileNo(e.target.value)}
		                />
		            </Form.Group>

		         	
	        	    	<Button className="me-2" variant="success" type="submit" id="submitBtn">
	        	    		Update
	        	    	</Button>
	        	         
	        	    	<Button className="my-2"as={Link} to="/admin" variant="danger">
	        	    		Cancel
	        	    	</Button>

	        	  

		        </Form>
		        </Card>
	
    			

    			</div>	
    			{/*Right Side End*/}

    		</div>
    		{/*<!-- row -->*/}
    	</div>	
    	</Container>
		</>
		:
		<Navigate to="/products" />
		
	)
}
