import React from "react";
import { Link } from 'react-router-dom';
import { FaFacebook } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaYoutube } from "react-icons/fa";


const Footer = () => <footer className="page-footer text-white pt-4">
    <div className="container-fluid text-center text-md-left bg-dark text-white pt-5">
        <div className="row">
            <div className="col-md-6 mt-md-0 mt-3 pb-3 text-white">
                <h5 className="text-uppercase">Mrs.Nemis dry-Sotanghon</h5>
                <p>Offering you the best food bilao and trays in town! <br /> We also offer food deliveries and catering services.</p>
                
                 <ul className="list-unstyled text-white" >
                    <li><Link style={{textDecoration: 'none'}}>Located at Brgy. Sirang Lupa, Calamba City, Laguna Philippines</Link></li>
                    
                </ul>
                <FaFacebook className="p-2" style={{ fontSize: '50px'}}/><FaInstagram className="p-2" style={{ fontSize: '50px'}}/><FaYoutube className="p-2" style={{ fontSize: '50px'}}/>
            </div>

            <hr className="clearfix w-100 d-md-none pb-0"/>

            <div className="col-md-3 mb-md-0 mb-3">
                <h5 className="text-uppercase">Catering Services</h5>
                <ul className="list-unstyled">
                    <li><Link style={{textDecoration: 'none'}}>Birthday Parties</Link></li>
                    <li><Link style={{textDecoration: 'none'}}>Wedding Events</Link></li>
                    <li><Link style={{textDecoration: 'none'}}>Company Outings</Link></li>
                    <li><Link style={{textDecoration: 'none'}}>Trainings and Seminars</Link></li>
                </ul>
            </div>

            <div className="col-md-3 mb-md-0 mb-3">
                <h5 className="text-uppercase">Products</h5>
                <ul className="list-unstyled">
                    <li><Link style={{textDecoration: 'none'}}>Food Bilao</Link></li>
                    <li><Link style={{textDecoration: 'none'}}>Food Trays</Link></li>
                    <li><Link style={{textDecoration: 'none'}}>Catering Services</Link></li>
                    <li><Link style={{textDecoration: 'none'}}>Food Deliveries</Link></li>
                </ul>
            </div>
        </div>
    </div>

    <div className="footer-copyright text-center bg-dark text-white py-3">© 2022 Copyright:
        <Link style={{textDecoration: 'none'}}> Mrs.Nemis Dry-Sotanghon</Link>
    </div>

</footer>

export default Footer