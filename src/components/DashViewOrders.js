
import { useContext, useState, useEffect } from "react";
import {Table, Button, Container} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import { FaUsers } from "react-icons/fa";
import { FaShoppingBag } from "react-icons/fa";
import { FaShoppingCart } from "react-icons/fa";
import { FaUsersCog } from "react-icons/fa";


export default function ViewOrders (){

	const {user} = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);


	function formatDate(isoDate) {
  	return (new Date(isoDate)).toLocaleString(
    'en-PH',
    {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false
    }
  )
};


	const fetchData = () =>{
	
		fetch(`https://backend-csp3-nemis.onrender.com/orders/`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{formatDate(order.purchasedOn)}</td>
						<td>{order.deliveryMode}</td>
						<td>{order.deliveryAddress}</td>
						<td>{order.orderStatus}</td>
						<td>{order.totalAmount}</td>
						<td className="tablecol">
							
							<Button as={ Link } to={`/${order._id}`} variant="success" className="my-3 mx-3" size="sm" onClick ="">View Details</Button>
									
							</td>
					</tr>
				)
			}))

		})
	}


	useEffect(()=>{
		
		fetchData();
	})


	const [firstName, setFirstName] = useState("");

	
	useEffect(()=>{
		
		fetch('https://backend-csp3-nemis.onrender.com/users/userDetails', {
          headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{

			console.log(data);

		
			setFirstName(data.firstName);
		

		})

	},[])


	return(
	
		(user.isAdmin)
		?
		<>
			{/*Main Content Section Start*/}
			<Container className="dashcon container-fluid">
    	<div className="  text-center pt-5">
    		{/*<!-- row -->*/}
    		<div className="row">
    					{/*<!-- Side-bar Section Start -->*/}

    			<div className=" card col-xl-3 d-none d-xl-block pt-5 ">
    				<div>
    					{/*<!-- Users Menu Start-->*/}
    					<h3>Admin Dashboard</h3>
    					<h4 className="pb-4">Welcome {firstName}!</h4>
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">

    								<Link to="/admin/retrieveallusers" style={{textDecoration: 'none'}} className="text-light p-2"><FaUsers className="p-2" style={{ fontSize: '60px'}}/>
    									Users
    								</Link>

    							</h4> 
    						</div>
  
    					</div>
    					{/*<!-- Users Menu End-->*/}

    					{/*<!-- Products Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin" className="text-light" style={{textDecoration: 'none'}}><FaShoppingBag className="p-2" style={{ fontSize: '60px'}}/>

    									Products
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Products Menu End -->*/}

    					{/*<!-- Orders Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to="/admin/viewOrders" className="text-light" style={{textDecoration: 'none'}} ><FaShoppingCart className="p-2" style={{ fontSize: '60px'}}/>

    									Orders
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- Orders Menu End -->*/}

    					{/*<!-- My Account Menu Start -->*/}
    					<div className="card">
    						<div className="p-4 bg-dark">
    							<h4 className="mb-0">
    								<Link to={`/admin/adminAccount`} className="text-light" style={{textDecoration: 'none'}}><FaUsersCog className="p-2" style={{ fontSize: '60px'}}/>

    									My Account
    								</Link>
    							</h4>
    						</div>
    					</div>
    					{/*<!-- My Account Menu End -->*/}
    				</div>
    			</div>
    			{/*Side Bar End*/}

    			{/*Right Side Start*/}
    			<div className="col-xl-9 pt-5">

    			<div className="prodtext">
    			<h1 >View Orders</h1>
    			</div>

			
    				{/*Table Start*/}
    				<div className=" min-vh-100  p-md-3">
    					<Table striped bordered hover size="sm">
						     <thead className=" bg-dark text-light">
						       <tr className="tabhead">
						         <th>Order ID</th>
						         <th>Purchase Date</th>
						         <th>Delivery Mode</th>
						         <th>Delivery Address</th>
						         <th>Order Status</th>
						         <th>Total Amount</th>
						         <th>Action</th>
						       </tr>
						     </thead>
						     <tbody>
						       { allOrders}
						     </tbody>
						   </Table>
    				</div>
    				{/*Table End*/}

    			</div>	
    			{/*Right Side End*/}

    		</div>
    		{/*<!-- row -->*/}
    	</div>	
    	</Container>
		</>
		:
		<Navigate to="/products" />
		
	)
}
