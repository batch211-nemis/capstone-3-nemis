import { Link } from 'react-router-dom';
import { Card, Button } from 'react-bootstrap';


export default function ProductCard({productProp}){

	let { name, description, price, photo, _id } = productProp;


return (
  <div>
    <Card style={{width:"100%", height:"100%"}}>
      <Card.Body>
        <img className="photo" src={photo} alt={name}/>
        <Card.Title className="pt-3 fw-bolder">{name}</Card.Title>
        <Card.Subtitle className="pt-3 fw-bolder" >Description:</Card.Subtitle>
        <Card.Text className="card-text">
          {description}
        </Card.Text>
        <Card.Subtitle className="fw-bolder pt-3">Price:</Card.Subtitle>
		<Card.Text className="fw-bolder">PHP {price}</Card.Text>
      </Card.Body>
     <Card.Footer>
		<Button as={Link} to={`/products/${_id}`}>Details</Button>
	</Card.Footer>
    </Card >
  
    </div>

  )

}

