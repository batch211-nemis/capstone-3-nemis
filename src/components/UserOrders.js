import { useContext, useState, useEffect } from "react";
import { Table, Button } from "react-bootstrap";
import { Navigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import { FaUsersCog } from "react-icons/fa";


export default function Dash(){

	const {user} = useContext(UserContext);

	const [allOrders, setAllOrders] = useState([]);


function formatDate(isoDate) {
  	return (new Date(isoDate)).toLocaleString(
    'en-PH',
    {
      year: 'numeric',
      month: 'short',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false
    }
  )
};


	const fetchData = () =>{

		fetch(`https://backend-csp3-nemis.onrender.com/orders/getUserOrders`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setAllOrders(data.map(order => {
				return(
					<tr key={order._id}>
						<td>{order._id}</td>
						<td>{formatDate(order.purchasedOn)}</td>
						<td>{order.deliveryMode}</td>
						<td>{order.deliveryAddress}</td>
						<td>{order.orderStatus}</td>
						<td>Php {order.totalAmount}</td>
						<td>
							<Button className="mx-3" as={ Link } to={`/${order._id}`} variant="success" size="sm"  onClick ="">View</Button>
							{/*<Button as={Link} to="/products2" variant="warning" size="sm"  onClick ="">Add Items</Button>*/}
						</td>
					</tr>
				)
			}))
		})
	}

	
	useEffect(()=>{
		
		fetchData();
	})


	
	const [firstName, setFirstName] = useState("");

	useEffect(()=>{
		
		fetch('https://backend-csp3-nemis.onrender.com/users/userDetails', {
          headers: {
            "Authorization": `Bearer ${localStorage.getItem("token")}`
          }
        })
        .then(res=>res.json())
        .then(data=>{

			console.log(data);

			setFirstName(data.firstName);

		})

	},[])



	return(

		(user.id !== null) && !(user.isAdmin)  
		?
		<>
			<div className="mt-5 mb-3 text-left pt-5 ">
			<h1>Welcome {firstName}!</h1>
			<h4><Link style={{textDecoration: 'none'}} to="/userAccount"><FaUsersCog className="p-2" style={{ fontSize: '50px'}}/>My Account</Link></h4>
				<hr />
				
				<h1 className="pb-1 text-center">My Orders</h1>

			
			</div>

			{(allOrders.length === 0) ?
				<div className="text-center pt-5 min-vh-100">
					<h1>You do not have existing orders yet. Go shopping now!</h1>
					<h1><Link to="/products">Go to Products</Link></h1>
				</div>
			:
			<div className="min-vh-100">
			<Table striped bordered hover className="text-center">
		     <thead className="bg-dark text-light">
		       <tr className="tabhead text-center">
		         <th>Order ID</th>
		         <th>Delivery Mode</th>
		         <th>Date of Purchase</th>
		         <th>Delivery Address</th>
		         <th>Status</th>
		         <th>Total Amount</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
		     </tbody>
		   </Table>

		   <div className="pb-5">
			<Button className="px-5" as={Link} to="/products" variant="primary" size="lg" style={{float: 'right'}} >Add Order</Button>
				</div>
				</div>
		}

		</>
		:
		(user.isAdmin) 
		?
			<Navigate to="/products" />
		:
			<Navigate to="/login" />
	)
}