import { useState, useEffect } from 'react';
import { Table, Container, Card, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

export default function ProductView(){

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [photo, setPhoto] = useState("");
	
	

	useEffect(()=>{
		
		
		fetch(`https://backend-csp3-nemis.onrender.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setPhoto(data.photo);
		})

	},[productId])


	const [viewPorder, setViewPorder] = useState([]);

	const fetchData = () =>{

		fetch(`https://backend-csp3-nemis.onrender.com/products/${productId}`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {

			const arr = data.orders;

			console.log(arr);

			setViewPorder(arr.map(item => {

				return (
					<tr>
						<td>{item.orderId}</td>
						<td>{item.quantity}</td>
						<td>{item.subTotal}</td>
					</tr>
					)
				}))
		  	})
	}
 					
     
useEffect(()=>{
		
		fetchData();
	})


	return(
		<Container>

		<div className="mt-5 pt-5 d-flex" style={{width:"100%", height:"100%"}}>
			<img className="photo2"  src={photo} alt={name}/>
		  <Row className="justify-content-center">
		     <Col className="px-5">
				<Card className="card" >
					<Card.Body >
						<Card.Title className="pt-3 fw-bolder">{name}</Card.Title>
						<Card.Subtitle className="pt-3 fw-bolder">Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle className="pt-3 fw-bolder">Price:</Card.Subtitle>
						<Card.Text className="fw-bolder">PHP {price}</Card.Text>
					</Card.Body>
				</Card>
		     </Col>
		  </Row>

		  
		  </div>

		  
		  {/*Table Start*/}
    				<div className=" min-vh-100  p-md-3 ">
    				<hr />
    					<Table striped bordered hover size="sm" style={{width: "70%"}} className="pTable">
						     <thead className=" bg-dark text-light">
						       <tr className="tabhead">
						         <th>Order ID</th>
						         <th>Quantity</th>
						         <th>Subtotal</th>
						       </tr>
						     </thead>
						     <tbody className="text-center">
						       { viewPorder }
						     </tbody>
						   </Table>
    				</div>
    		{/*Table End*/}
    	
		</Container>

	)
}

