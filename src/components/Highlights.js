import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import sotanghon from '../sotanghon.jpg';
import flumpia from '../flumpia.jpg';
import wings from '../wings.jpg';

export default function Highlights(){
  return(
    <Container>

    <h1 className="mt-4 mb-4 text-center">Our Best Sellers</h1>

    <Row className = "mt-3 mb-3">
    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3 text-center">
        <Card.Body>
          <img src={sotanghon} className="img-fluid pb-3" alt="Mrs.Nemis Dry Sotanghon"/>
          <Card.Title className="pb-2">Mrs. Nemis Dry-Sotanghon</Card.Title>
          <Card.Text>
          Our Bilao Best Seller. A Filipino glass noodle recipe that is mix with chopped chicken breast and vegetables. It is also season with soy sauce and oyster sauce for a better flavor of the noodle. Indeed, no party is complete without a pancit dish on the buffet table. One bilao good for 15-20 persons. Try our recipe and order now.
         </Card.Text>
         <Button className="align-center" as={Link} to="/products/6391c75f590270c178643582">Order Now</Button>
        </Card.Body>
      </Card>
    </Col>


    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3 text-center">
        <Card.Body>
        <img src={flumpia} className="img-fluid pb-3" alt="Mrs.Nemis Dry Sotanghon"/>
          <Card.Title className="pb-2">Fresh Lumpia</Card.Title>
          <Card.Text>
          This fresh lumpia recipe has everything you want in it: fresh veggies, pork, sauce, and even a fresh wrapper! Made with stir-fried vegetables, this Filipino fresh spring rolls with crepe wrapper vegetable filling, and a savory peanut sauce are a tasty and filling snack or light meal. The're nutritious as they're delicious!
         </Card.Text>
         <Button className="highlight-btn" as={Link} to="/products/6391cc1a590270c178646c09">Order Now</Button>
        </Card.Body>
      </Card>
    </Col>


    <Col xs={12} md={4}>
      <Card className="cardHighlight p-3 text-center">
        <Card.Body>
        <img src={wings} className="img-fluid pb-3" alt="Mrs.Nemis Dry Sotanghon"/>
          <Card.Title className="pb-2">Sweet Glazed Chicken Wings</Card.Title>
          <Card.Text>
          This Sweet Chili Glazed Chicken Wings recipe is so good it can be served up as dinner on top of some freshly steamed rice or as an appetizer eaten all on their own. They have a sweet flavor with just a tiny bit of spice. This makes a platter of wings that is perfect for sharing between four people. Try our recipe and order now. 
         </Card.Text>
         <Button className="highlight-btn" as={Link} to="/products/6391ce29590270c178648803">Order Now</Button>
        </Card.Body>
      </Card>
    </Col>
    </Row>
    </Container>
    );
}
