import Banner from '../components/Banner.js'
import { Container } from 'react-bootstrap';

export default function Error(){

	const data = {
		title: "404 - Page Not found",
		content: "The page you are looking cannot be found.",
		destination: "/",
		label: "Go Back Home"
	}

	return(
		<Container className="pt-5">
		<Banner bannerProp = {data}/>
		</Container>
		)
}