import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import bannerpic from '../bannerpic.png';
import { Link } from 'react-router-dom';


export default function Home(){

	const data = {
		title: "Mrs. Nemis Dry-Sotanghon",
		content: "Masarap na Pagkain, abot-kaya pa!",
		destination: "/products",
		label: "Order now!"
	}

	return(
		<>
		<section className="pt-5">
            <div className="pt-3">
            <Link to="/products"><img src={bannerpic} width="100%" alt="Mrs.Nemis Dry Sotanghon" onClick="/products"/></Link>
            </div> 
        </section>
			<Highlights/>
		</>
	)
}