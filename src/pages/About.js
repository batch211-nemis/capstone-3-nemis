import { Container, Card, Row, Col } from 'react-bootstrap';
import logo from '../logo.png';
import logo2 from '../logo2.png';
import bannerpic from '../bannerpic.png';
import review1 from '../review1.png';
import review2 from '../review2.png';
import review3 from '../review3.png';
import review4 from '../review4.png';
import { FaStar } from "react-icons/fa";

export default function AboutPage (){

	
	return(
		
		<Container>
		<div className="mt-5 pt-5 d-flex"  style={{width:"100%", height:"100%"}}>

			<img  className="photo3" src={logo} alt="Mrs.Nemis Dry-Sotanghon"/>
			 	
		   <Row className="justify-content-around">

			   <Col className="px-5">

				<Card className="card no-gutters shadow-outer p-3">
					<Card.Body >
					<div>
					</div>
						<Card.Title className="pt-2 fs-2 fw-bolder">Mrs.Nemis Dry-Sotanghon</Card.Title>
						<Card.Subtitle className=" pb-4 fs-4 fw-bolder">Masarap na, Abot Kaya Pa!</Card.Subtitle>
						<Card.Text className="fs-5">At Mrs. Nemis Dry-Sotanghon, we’re tied in with presenting delicious and affordable food, regardless of whether it implies going the additional mile. When you stroll through our menus, we do what we can to make every menu delivered and served right at your door. Mrs.Nemis Dry-Sotanghon is food business that is continually developing. From a simple sari-sari store and eatery in 2000, we’ve kept on evolving in order to help serve and offer you best food bilao and trays in town. We also offer now catering services for celebrations and special occasions like birthdays, anniversaries, wedding and even training or seminars. Today, we can deliver from different locations here in Calamba and some parts of Laguna. Be that as it may, regardless of where you discover us, quality will dependably be our formula. We Believe in Quality. All around. Quality food can’t be made without quality initiative. </Card.Text>
					</Card.Body>
				</Card>
		     </Col>
		  </Row>
		  </div>
		  <hr />
		  <br />

		  

		  	<div className="text-center d-flex">
			  <div>
			  	<h1 classname="p-5">Order Reviews <FaStar className="p-2" style={{ fontSize: '70px'}, { color: 'yellow'}}/><FaStar className="p-2" style={{ fontSize: '70px'}, { color: 'yellow'}}/><FaStar className="p-2" style={{ fontSize: '70px'}, { color: 'yellow'}}/><FaStar className="p-2" style={{ fontSize: '70px'}, { color: 'yellow'}}/><FaStar className="p-2" style={{ fontSize: '70px'}, { color: 'yellow'}}/></h1><br />
			  	<img  className="imh-fluid" src={review1} alt="Mrs.Nemis Dry-Sotanghon"/>
			  	<img  className="imh-fluid" src={review2} alt="Mrs.Nemis Dry-Sotanghon"/>
			  	<img  className="imh-fluid" src={review3} alt="Mrs.Nemis Dry-Sotanghon"/>
			  	<img  className="imh-fluid" src={review4} alt="Mrs.Nemis Dry-Sotanghon"/>
			  </div>
			  <div className="px-3">
			  	<img  className="img-fluid" src={logo2} alt="Mrs.Nemis Dry-Sotanghon"/><br />
			  	<img  className="img-fluid" src={bannerpic} alt="Mrs.Nemis Dry-Sotanghon"/>
			  </div>
			</div>

		  
		
		</Container>

	)
}




