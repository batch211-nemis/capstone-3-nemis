import React from 'react';
import Swal from 'sweetalert2';
import { useState, useEffect } from 'react';
import logo from '../logo.png';
import { Card, Form, Button } from 'react-bootstrap';
import { FaMapMarkerAlt } from "react-icons/fa";
import { FaAddressBook } from "react-icons/fa";
import { FaFacebook } from "react-icons/fa";
import { FaRegEnvelope } from "react-icons/fa";



export default function ContactForm () {


    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [message, setMessage] = useState("");
  
  const onSubmit = (e) => {

    e.preventDefault()
   
    let conFom = {
      name: name.value,
      email: email.value,
      message: message.value,
    }
    console.log(conFom);


   if(name !== null && email !== null && message !== null){
           Swal.fire({
                    title: "Your message was successfully sent!",
                    icon: "success",
                    text: `We will get in touch to you soon!`
                })

        } else {
            Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: `Something went wrong. Please try again later!`
                })
        }

    setName("");
    setEmail("");
    setMessage("");

}

  return (
    <div className="container mt-5 pt-5 d-flex"> 
    <img  className="photo5 p-3" src={logo} alt="Mrs.Nemis Dry-Sotanghon"/>
    <Card className="container-fluid p-4">
      <h2 className="mb-3 pt-2 fs-1">Contact Us</h2>
      <p className="fw-bold"><FaMapMarkerAlt className="p-1" style={{ fontSize: '30px'}}/>Location: Brgy. Sirang Lupa, Calamba City, Laguna Philippines <br />
      <FaAddressBook className="p-1" style={{ fontSize: '30px'}}/>Contact #: (0920) 668 4980 <br />
      <FaFacebook className="p-1" style={{ fontSize: '30px'}}/>Facebook: Mrs. Nemis Dry-Sotanghon <br />
      <FaRegEnvelope className="p-1" style={{ fontSize: '30px'}}/>Email: mrs.nemisdry-sotanghon@gmail.com</p>
      
      <Form onSubmit={(e) => onSubmit (e)}>
        <div className="mb-3">
          <label className="form-label" htmlFor="name">
            Name
          </label>
          <input className="form-control" type="text" id="name" required />
        </div>
        <div className="mb-3">
          <label className="form-label" htmlFor="email">
            Email
          </label>
          <input className="form-control" type="email" id="email" required />
        </div>
        <div className="mb-3">
          <label className="form-label" htmlFor="message">
            Message
          </label>
          <textarea className="form-control" id="message" required />
        </div>
        <Button className="btn btn-danger" type="submit">
          Send
        </Button>
      </Form>
      </Card>
    </div>
  )
}


