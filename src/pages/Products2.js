import { useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard2 from '../components/ProductCard2';
import UserContext from '../UserContext';
import { Container, Row } from 'react-bootstrap';

export default function Products(){

	
	const [products, setProducts] =useState([])

	const {user} = useContext(UserContext);
	console.log(user);

	
	useEffect(()=>{
		
	fetch(`https://backend-csp3-nemis.onrender.com/products/`)
	.then(res=>res.json())
	.then(data=>{

		
	const productArr = (data.map(product => {
		return (
			
			<ProductCard2 productProp={product} key={product._id}/>
			)
		}))
		setProducts(productArr)
	})

	},[products])

	
	return(
		(user.isAdmin)?
		<Navigate to="/admin"/>
		:
		<>
		<Container className="pt-5 pb-3">
		
			<h1 className="pt-5">Our Menu</h1>
			<hr />
			
			<div >
			<Row xs={1} md={3} className="g-4" >
			{products}
			</Row>
			</div>
			
		
		</Container>
		</>
	)
}
