import {Form, Button} from 'react-bootstrap';
import {Row, Col, Card} from 'react-bootstrap';
import { Link } from 'react-router-dom';
//Complete (3) Hooks of React
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Login() {


  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [isActive, setIsActive] = useState ('');

  console.log(email);
  console.log(password);

  const { user, setUser } = useContext(UserContext);


  function authenticate(e) {

      e.preventDefault()


      fetch('https://backend-csp3-nemis.onrender.com/users/login', {
        method: 'POST',
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data);
        if(typeof data.access !== "undefined"){
          localStorage.setItem('token',data.access)
          retrieveUserDetails(data.access)

          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to Mrs. Nemis Dry Sotanghon!"
          });
          
        } else {
          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Check your credentials!"
          })
        }
      })


      const retrieveUserDetails = (token) =>{

        fetch('https://backend-csp3-nemis.onrender.com/users/userDetails', {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
          console.log(data);

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
      }


      setEmail("");
      setPassword("");
  }

  useEffect(()=>{

    if(email !== "" && password !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
 

  }, [email, password])


  return (
    (user.id !== null) && !(user.isAdmin)
    ?
    <Navigate to="/userOrders"/>
    :
    (user.isAdmin) 
    ?
    <Navigate to="/admin"/>
    :
    <Row className = " justify-content-center p-5 mt-5 mb-3">
    <Col  xs={12} md={6}>
      <Card className="cardHighlight p-3 ">
        <Card.Body>
          <Card.Title className="text-center"><h1>Login to your account</h1></Card.Title>
             <Card.Text className="text-center">Don't have an account yet? <Link to="/register">Register</Link></Card.Text>
                <Form className="p-3" onSubmit={(e)=>authenticate(e)}>
                  <Form.Group className="mb-3" controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                    type="email"
                    placeholder="Enter your email"
                    value={email}
                    onChange={e=>setEmail(e.target.value)}
                    required
                    />
                  </Form.Group>

                  <Form.Group className="mb-3" controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={e=>setPassword(e.target.value)}
                    required
                    />
                  </Form.Group>
                  
                  { isActive ?
                  <Button variant="success" type="submit" id="submitBtn">
                    Login
                  </Button>
                  :
                  <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Login
                  </Button>
                  }
                </Form>
        </Card.Body>
      </Card>
    </Col>
  </Row>

  )
}
